package junittask;

import junittask.plateau.Plateau;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PlateauTestMockito {
    private static Logger LOG = LogManager.getLogger(PlateauTest.class);
    private static Plateau realPlateau = new Plateau();

    @Mock
    Plateau plateauMock;

    @DisplayName("Mock test 1")
    @Test
    void testMethod1() {
        LOG.info("First test(mockito) running.");
        when(plateauMock.getSequence()).thenReturn("hi from mockito");
        assertEquals(plateauMock.getSequence(), "hi from mockito");
    }

    @DisplayName("Mock test 2")
    @Test
    void testMethod2() {
        LOG.info("Second test(mockito) running.");
        when(plateauMock.getSequence()).thenReturn(realPlateau.getSequence());
        assertEquals(plateauMock.getSequence(), realPlateau.getSequence());
    }

}